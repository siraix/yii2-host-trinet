<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2-host',
    'username' => 'yii2-host',
    'password' => 'yii2-host',
    'charset' => 'utf8'
];
