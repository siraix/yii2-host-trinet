<?php

use yii\db\Migration;

/**
 * Class m190730_171952_insert_data_to_users_Table
 */
class m190730_171952_insert_data_to_users_Table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i <= 5000; $i++) {
            $this->insert('{{%users}}', [
                'login' => $faker->firstName . $faker->lastName . rand(1, 31) . rand(1, 12) . rand(1960, 2019),
                'email' => $faker->userName . "@email.com",
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%users}}', ['between', 'id', '1', '5000']);
    }
}
